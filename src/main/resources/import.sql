/* Creamos algunos usuarios con sus roles */
INSERT INTO role (role, status) VALUES ('ROLE_USER', 1);
INSERT INTO role (role, status) VALUES ('ROLE_ADMIN', 1);

INSERT INTO users (name, last_name, username, password, status, role_id) VALUES ('andres','lopez','a@w.com','$2a$10$O9wxmH/AeyZZzIS09Wp8YOEMvFnbRVJ8B4dmAMVSGloR62lj.yqXG',1,1);
INSERT INTO users (name, last_name, username, password, status, role_id) VALUES ('admin','lopez','amd@w.com','$2a$10$DOMDxjYyfZ/e7RcBfUpzqeaCs8pLgcizuiQWXPkU35nOhZlFcE9MS',1,2);

INSERT INTO bank (name, status) VALUES ('Bank of America', 1);
INSERT INTO bank (name, status) VALUES ('Bank Panama', 1);

INSERT INTO type_identification (name, status) VALUES ('J', 1);
INSERT INTO type_identification (name, status) VALUES ('V', 1);

INSERT INTO commerce (address, created_date, email, full_name, identification, name_commerce, phone, status, updated_date, url_image) VALUES ('Caracas', '2020-03-25 07:21:23', 'cc@email.com', 'Pedro Perez', '5874563211', 'Super Market', '+584245871463', b'1', NULL, '/home/wallet/public_html/images/codes/');