package com.app.wallet.controllers;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.app.wallet.models.entity.Commerce;
import com.app.wallet.models.entity.Users;
import com.app.wallet.models.service.ICommerceService;
import com.app.wallet.models.service.IUsersService;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.app.wallet.GenerateCodeQR;

@CrossOrigin(origins = { "http://localhost:3000", "http://localhost:4200" })
@RestController
@RequestMapping("/api/user")
public class UserController {

	@Autowired
	private IUsersService iUsersService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;

	@GetMapping(value = "/list")
	@Secured({ "ROLE_ADMIN", "ROLE_USER" })
	public List<Users> list() {
		return iUsersService.usersActive();
	}

	@RequestMapping(value = "/save", produces = MediaType.APPLICATION_JSON_VALUE)
	@Secured({ "ROLE_ADMIN" })
	public Map<String, String> save(@RequestBody Users user, HttpServletRequest request,
			HttpServletResponse response) throws JsonParseException, JsonMappingException, IOException {

		/*
		 * { "id": 1, "fullName": "retretret", "nameCommerce": "retertret", "rif":
		 * "654654656", "address": "tertertre", "phone": "5646546546546", "email":
		 * "tretretret", "status": true, "bank": { "id": 1, "name": "Bank of America",
		 * "status": true }, "typeIdentification": { "id": 1, "name": "J", "status":
		 * true } }
		 */

		HashMap<String, String> map = new HashMap<>();
		//GenerateCodeQR codeQR = new GenerateCodeQR();

		try {
			
			/*Integer randomNum = ThreadLocalRandom.current().nextInt(100000, 900000);
			//System.out.println("randomNum "+randomNum);
			String token = passwordEncoder.encode(randomNum.toString());
			//System.out.println("bcryptCode "+token);
			user.setToken(token);*/
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			user.setStatus(true);
			iUsersService.save(user);
			map.put("mensaje", "Guardado con Éxito");
			
			/*Email sendEmail = new Email();
			sendEmail.welcomeMail(users.getUsername(), users.getName()+" "+users.getLastName(), "", "");*/

			return map;

		} catch (Exception e) {
			// TODO: handle exception
			response.sendError(HttpServletResponse.SC_BAD_REQUEST, "Error guardando el registro, revise los datos ");
			map.put("error", "Error guardando el registro, revise los datos");
			return map;
		}

	}
}
