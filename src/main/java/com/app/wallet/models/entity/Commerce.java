package com.app.wallet.models.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@Entity
@Table(name = "commerce")
public class Commerce implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	
	private String fullName;
	
	private String nameCommerce;

	private String identification;
	
	private String address;
	
	private String phone;
	
	private String email;
	
	private String urlImage;
	
	private Boolean status = true;
	
	/*@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="bank_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private Bank bank;
	
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="type_identification_id")
	@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
	private TypeIdentification typeIdentification;*/
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "created_date")
	private Date createdDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss", locale="VE", timezone = "America/Caracas")
	@Column(name = "updated_date")
    private Date updatedDate;
	
	@PrePersist
	public void prePersist() {
		createdDate = new Date();
	}
 
    @PreUpdate
    public void preUpdate() {
    	updatedDate = new Date();
    }

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;



	public Long getId() {
		return id;
	}



	public void setId(Long id) {
		this.id = id;
	}



	public String getFullName() {
		return fullName;
	}



	public void setFullName(String fullName) {
		this.fullName = fullName;
	}



	public String getNameCommerce() {
		return nameCommerce;
	}



	public void setNameCommerce(String nameCommerce) {
		this.nameCommerce = nameCommerce;
	}



	public String getIdentification() {
		return identification;
	}



	public void setIdentification(String identification) {
		this.identification = identification;
	}



	public String getAddress() {
		return address;
	}



	public void setAddress(String address) {
		this.address = address;
	}



	public String getPhone() {
		return phone;
	}



	public void setPhone(String phone) {
		this.phone = phone;
	}



	public String getEmail() {
		return email;
	}



	public void setEmail(String email) {
		this.email = email;
	}



	public Boolean getStatus() {
		return status;
	}



	public void setStatus(Boolean status) {
		this.status = status;
	}



	/*public Bank getBank() {
		return bank;
	}



	public void setBank(Bank bank) {
		this.bank = bank;
	}



	public TypeIdentification getTypeIdentification() {
		return typeIdentification;
	}*/



	public Date getCreatedDate() {
		return createdDate;
	}

	public Date getUpdatedDate() {
		return updatedDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}

	/*public void setTypeIdentification(TypeIdentification typeIdentification) {
		this.typeIdentification = typeIdentification;
	}*/

	public String getUrlImage() {
		return urlImage;
	}

	public void setUrlImage(String urlImage) {
		this.urlImage = urlImage;
	}

}
