package com.app.wallet.models.service;

import java.util.List;

import com.app.wallet.models.entity.Commerce;

public interface ICommerceService {
	
	public List<Commerce> findAll();
	
	public List<Commerce> commerceActive();
	
	public void save(Commerce commerce);
	
	public void delete(Long id);
	
	public Commerce commerce(Long id);

}
