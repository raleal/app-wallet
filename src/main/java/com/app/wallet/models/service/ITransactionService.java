package com.app.wallet.models.service;

import java.util.List;

import com.app.wallet.models.entity.Commerce;
import com.app.wallet.models.entity.Transaction;

public interface ITransactionService {
	
	public List<Transaction> findAll();
	
	public List<Transaction> transactionActive();
	
	public void save(Transaction transaction);
	
	public void delete(Long id);
	
	public Transaction transaction(Long id);

}
