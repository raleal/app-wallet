package com.app.wallet.models.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.app.wallet.models.dao.ICommerceDao;
import com.app.wallet.models.dao.ITransactionDao;
import com.app.wallet.models.entity.Commerce;
import com.app.wallet.models.entity.Transaction;
import com.app.wallet.models.service.ICommerceService;
import com.app.wallet.models.service.ITransactionService;

@Service
public class ITransactionImpl implements ITransactionService{
	
	@Autowired
	private ITransactionDao iTransactionDao;

	@Override
	public List<Transaction> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Transaction> transactionActive() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void save(Transaction transaction) {
		// TODO Auto-generated method stub
		iTransactionDao.save(transaction);
	}

	@Override
	public void delete(Long id) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Transaction transaction(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
